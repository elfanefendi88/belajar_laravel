<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;

class PegawaiController extends Controller
{
    public function index()
    {
    	// mengambil data pegawai
    	//$pegawai = Pegawai::all();
		
		$data['all'] = Pegawai::all();
		$data['first'] = Pegawai::first();
		$data['find'] = Pegawai::find(5); 
		$data['where'] = Pegawai::where('nama', 'Jamalia Susanti M.TI.')->get();
		$data['where2'] = Pegawai::where('id', '>=' , 10)->get();
		$data['where3'] = Pegawai::where('nama', 'like' , '%ma%')->get();
 
    	// mengirim data pegawai ke view pegawai
    	//return view('pegawai', ['all' => $data['all'], 'first' => $data['first']]);
    	return view('pegawai', $data);
    }
}
