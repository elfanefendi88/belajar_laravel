<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
		
		Gate::define('manage-users', function($user){
			// TODO: logika untuk mengizinkan manage users
			if($user->status == 'ACTIVE'){
				return count(array_intersect(["ADMIN"], json_decode($user->roles)));
			}else{
				return false;
			}
		});
		
		Gate::define('manage-categories', function($user){
			// TODO: logika untuk mengizinkan manage categories
			if($user->status == 'ACTIVE'){
				return count(array_intersect(["ADMIN", "STAFF"], json_decode($user->roles)));
			}else{
				return false;
			}
		});
		
		Gate::define('manage-books', function($user){
			if($user->status == 'ACTIVE'){
				return count(array_intersect(["ADMIN", "STAFF"], json_decode($user->roles)));
			}else{
				return false;
			}
			// TODO: logika untuk mengizinkan manage books
		});
		
		Gate::define('manage-orders', function($user){
			if($user->status == 'ACTIVE'){
				return count(array_intersect(["ADMIN", "STAFF"], json_decode($user->roles)));
			}else{
				return false;
			}
			// TODO: logika untuk mengizinkan manage orders'
		});
		
		Gate::define('manage-customers', function($user){
			if($user->status == 'ACTIVE'){
				return count(array_intersect(["CUSTOMER"], json_decode($user->roles)));
			}else{
				return false;
			}
			// TODO: logika untuk mengizinkan manage orders'
		});
    }
}
