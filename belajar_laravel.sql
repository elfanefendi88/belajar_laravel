-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2020 at 10:57 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(11) NOT NULL,
  `judul_ar` text NOT NULL,
  `isi_ar` text NOT NULL,
  `keterangan_ar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul_ar`, `isi_ar`, `keterangan_ar`) VALUES
(1, 'Cerita Si Kancil', 'Dahulu Kala..', 'Cerita Dongeng'),
(2, 'Misteri Raja Bajak Laut Gold', 'Dikasahkan Seorang Raja Dari Blue Sea...', 'Cerita sampingan One Piece Gold'),
(3, 'Test Judul', 'Test Isi', 'Test Keterangan'),
(4, 'Gina Puspita', 'molestiae', 'Gg. Baja Raya No. 570, Palopo 62068, Gorontalo'),
(5, 'Ajimat Sabri Wibisono M.Kom.', 'dolorem', 'Kpg. Uluwatu No. 745, Singkawang 28669, KalUt'),
(6, 'Nasab Nashiruddin', 'minima', 'Jln. Bakti No. 571, Bandar Lampung 95700, NTT'),
(7, 'Asmuni Darman Habibi', 'ab', 'Jln. Raya Ujungberung No. 935, Administrasi Jakarta Pusat 91283, JaTeng'),
(8, 'Mila Astuti S.E.I', 'labore', 'Dk. Nanas No. 874, Tomohon 62470, DKI'),
(9, 'Luis Dongoran S.Sos', 'at', 'Jr. Babadan No. 945, Sukabumi 60167, KalUt'),
(10, 'Hamzah Waskita', 'ut', 'Psr. Ketandan No. 386, Blitar 45288, SumBar'),
(11, 'Mumpuni Sitompul', 'consequatur', 'Kpg. Setiabudhi No. 786, Pangkal Pinang 14642, DKI'),
(12, 'Edison Uwais', 'sed', 'Ki. S. Parman No. 777, Blitar 12587, Maluku'),
(13, 'Yoga Zulkarnain S.Sos', 'voluptatem', 'Dk. Bakaru No. 182, Bengkulu 32683, JaTeng'),
(14, 'Raina Azalea Palastri', 'rerum', 'Ki. Madiun No. 576, Banjar 36128, Banten'),
(15, 'Harja Damu Sihotang M.Pd', 'ab', 'Gg. Perintis Kemerdekaan No. 839, Palangka Raya 53969, Gorontalo'),
(16, 'Hadi Suwarno', 'numquam', 'Jr. Baja Raya No. 525, Jayapura 66904, DKI'),
(17, 'Mila Lestari', 'facere', 'Ds. Pasirkoja No. 955, Tangerang Selatan 35877, JaTim'),
(18, 'Yessi Pudjiastuti', 'dolorum', 'Ds. Yos Sudarso No. 235, Palopo 75829, Bengkulu'),
(19, 'Jais Santoso S.Kom', 'asperiores', 'Kpg. Baiduri No. 838, Solok 97460, Maluku'),
(20, 'Jindra Narpati', 'sint', 'Psr. Basuki Rahmat  No. 82, Singkawang 76863, KepR'),
(21, 'Vera Usada', 'ipsa', 'Ki. Setia Budi No. 729, Tangerang 31544, Papua'),
(22, 'Latif Gunarto', 'accusantium', 'Dk. Baladewa No. 144, Banjar 34397, JaTim'),
(23, 'Chelsea Devi Puspasari S.E.I', 'ea', 'Gg. Yogyakarta No. 506, Bima 35506, NTT'),
(24, 'Belinda Nuraini', 'placeat', 'Gg. Merdeka No. 184, Administrasi Jakarta Timur 17753, SulUt'),
(25, 'Daliman Jati Irawan M.Pd', 'iusto', 'Kpg. Banda No. 519, Jayapura 85700, SulBar'),
(26, 'Nasab Jailani', 'aspernatur', 'Kpg. Babah No. 915, Mojokerto 50706, SulBar'),
(27, 'Titin Puspasari', 'voluptates', 'Ds. Babadan No. 533, Langsa 19435, SumBar'),
(28, 'Azalea Purwanti S.Farm', 'ipsa', 'Ds. Supono No. 787, Administrasi Jakarta Utara 82847, JaTim'),
(29, 'Hardana Ibrahim Latupono', 'voluptatem', 'Kpg. Yogyakarta No. 975, Tangerang Selatan 85046, Gorontalo'),
(30, 'Sabrina Purnawati', 'eum', 'Jln. Laswi No. 931, Subulussalam 12788, SumSel'),
(31, 'Padma Usada', 'quam', 'Ds. Ciumbuleuit No. 780, Sibolga 90746, SulUt'),
(32, 'Rahmi Mulyani M.Pd', 'quidem', 'Dk. Sudirman No. 200, Manado 58567, PapBar'),
(33, 'Nurul Padmi Wahyuni S.I.Kom', 'ut', 'Dk. Jaksa No. 363, Tangerang Selatan 42605, JaBar'),
(34, 'Zamira Humaira Kusmawati', 'quibusdam', 'Gg. Bakit  No. 695, Ambon 73438, Jambi'),
(35, 'Puput Septi Pertiwi S.Pd', 'rerum', 'Jln. Sunaryo No. 151, Tebing Tinggi 61687, JaTim'),
(36, 'Rahayu Yuniar', 'ut', 'Ki. Suniaraja No. 703, Tanjungbalai 19127, MalUt'),
(37, 'Tiara Ana Suartini S.E.I', 'dignissimos', 'Psr. Diponegoro No. 237, Singkawang 87237, KalTeng'),
(38, 'Sadina Wahyuni', 'est', 'Psr. Sugiono No. 486, Administrasi Jakarta Utara 93079, NTT'),
(39, 'Cengkal Winarno', 'quo', 'Kpg. Taman No. 875, Pekalongan 89278, KalSel'),
(40, 'Cager Tarihoran', 'ut', 'Ds. Bakin No. 749, Padang 82783, SumBar'),
(41, 'Dadap Gaman Situmorang', 'laboriosam', 'Ds. Abang No. 702, Metro 82145, Maluku'),
(42, 'Mulya Putra', 'ut', 'Ds. Bara Tambar No. 203, Dumai 23779, SumSel'),
(43, 'Paris Hastuti', 'asperiores', 'Ds. Madrasah No. 610, Singkawang 97593, SumSel'),
(44, 'Mursita Taufik Mandala', 'mollitia', 'Jln. Padang No. 896, Administrasi Jakarta Barat 62200, KalSel'),
(45, 'Almira Andriani S.IP', 'odit', 'Ds. Cemara No. 509, Balikpapan 78300, Bali'),
(46, 'Cindy Widiastuti', 'voluptatum', 'Jr. Daan No. 934, Bontang 20577, KepR'),
(47, 'Heryanto Sirait', 'enim', 'Jr. Zamrud No. 404, Bandar Lampung 37589, Maluku'),
(48, 'Vega Siregar', 'ut', 'Kpg. Basoka No. 603, Tanjungbalai 99364, SulBar'),
(49, 'Warta Simbolon', 'repellat', 'Jr. Achmad No. 185, Yogyakarta 44045, SumUt'),
(50, 'Amalia Yulianti', 'sed', 'Gg. Antapani Lama No. 164, Mojokerto 92360, Jambi'),
(51, 'Eka Ajeng Farida S.Sos', 'ipsa', 'Jln. Umalas No. 556, Palopo 22149, Papua'),
(52, 'Bakianto Sitompul', 'velit', 'Ds. Sutan Syahrir No. 895, Pematangsiantar 41074, Banten'),
(53, 'Cahyo Wahyudin', 'minus', 'Psr. Baan No. 234, Banjarbaru 65834, Riau');

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id_barang` int(11) NOT NULL,
  `nama_bar` varchar(100) NOT NULL,
  `jumlah_bar` int(11) NOT NULL,
  `tanggal_update` datetime NOT NULL,
  `keterangan_bar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id_barang`, `nama_bar`, `jumlah_bar`, `tanggal_update`, `keterangan_bar`) VALUES
(1, 'Tissue', 100, '2020-03-24 10:39:09', 'Tissue Paseo');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publisher` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `views` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `stock` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` enum('PUBLISH','DRAFT') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `slug`, `description`, `author`, `publisher`, `cover`, `price`, `views`, `stock`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'bpupki', 'bpupki', 'buku history pemerintah', 'indo', 'elex media', 'book-covers/UHazs6COdVRAIueNIMVRjpvI24m3dHJsHabz1jbt.png', 20000.00, 0, 4, 'PUBLISH', 2, NULL, NULL, '2020-04-05 21:36:04', '2020-04-06 00:50:44', '2020-04-06 00:50:44'),
(2, 'senjata corona', 'senjata-corona', 'ancaman virus', 'orang asing', 'gramedia', 'book-covers/8qmyklNrXTSnKTmoj0vASbySE9mRAc1h2KE6ihOj.jpeg', 450000.00, 0, 2, 'DRAFT', 2, NULL, NULL, '2020-04-05 21:55:51', '2020-04-05 21:55:51', NULL),
(3, 'adskjnl', 'adskjnl', 'asd', 'indo', 'gramedia', 'book-covers/IBlYSL8seeW6EOX9qFyV9Dm5GZx5dPShSVxSnGPz.jpeg', 2300.00, 0, 2, 'PUBLISH', 2, NULL, NULL, '2020-04-06 00:07:53', '2020-04-06 00:07:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_category`
--

CREATE TABLE `book_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_category`
--

INSERT INTO `book_category` (`id`, `book_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 2, NULL, NULL),
(2, NULL, 3, NULL, NULL),
(3, NULL, 2, NULL, NULL),
(4, 3, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_order`
--

CREATE TABLE `book_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'berisi nama file image saja tanpa path',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `image`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Perabotan Rumah Tangga2', 'perabotan-rumah-tangga2', 'category_images/joPPTSdFvhIT1u0VBUvBWRAEk6SRakxxOjK22US9.gif', 2, 2, NULL, '2020-04-02 00:39:53', '2020-04-01 23:07:28', '2020-04-02 00:39:53'),
(2, 'sejarah', 'sejarah', 'category_images/AdnbQhBZwKYFUxMDcRbLJvNk8b8Fm7iH0OXvSxXj.jpeg', 2, NULL, NULL, NULL, '2020-04-05 21:23:46', '2020-04-05 21:23:46'),
(3, 'biologi', 'biologi', 'category_images/QZEUstQE55v2L3X6TFRi1346Lhhgx1pCcYpmJgLK.jpeg', 2, NULL, NULL, NULL, '2020-04-05 21:54:47', '2020-04-05 21:54:47');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `umur` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `nama`, `umur`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Fitria Gilda Hastuti S.Pd', 40, NULL, NULL, NULL),
(3, 'Ana Mayasari S.Gz', 34, NULL, NULL, NULL),
(4, 'Cemeti Gunarto', 33, NULL, NULL, NULL),
(5, 'Gangsa Samosir', 41, NULL, NULL, NULL),
(6, 'Salwa Wastuti', 44, NULL, NULL, NULL),
(7, 'Cici Oliva Puspita', 37, NULL, NULL, NULL),
(8, 'Dartono Kusumo', 36, NULL, NULL, NULL),
(9, 'Hardi Asmadi Uwais S.Ked', 26, NULL, NULL, NULL),
(10, 'Gina Halimah', 39, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2020_03_30_030719_create_pegawais_table', 1),
(5, '2014_10_12_000000_create_users_table', 2),
(6, '2014_10_12_100000_create_password_resets_table', 2),
(7, '2019_08_19_000000_create_failed_jobs_table', 2),
(8, '2020_03_31_075025_penyesuaian_table_users', 3),
(9, '2020_04_02_035957_create_table_categories', 4),
(10, '2020_04_03_061550_create_books_table', 5),
(11, '2020_04_03_061957_create_book_category_table', 6),
(12, '2020_04_07_022906_create_orders_table', 7),
(13, '2020_04_07_024051_create_book_order_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `total_price` double(8,2) UNSIGNED NOT NULL,
  `invoice_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('SUBMIT','PROCESS','FINISH','CANCEL') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 'Yessi Dewi Palastri S.Kom', 'Ds. Ters. Buah Batu No. 978, Sabang 91500, JaTim', NULL, NULL),
(2, 'Gamani Saputra', 'Jln. Peta No. 454, Cilegon 18147, KalBar', NULL, NULL),
(3, 'Sari Andriani', 'Jr. Dipatiukur No. 934, Metro 53720, SulTra', NULL, NULL),
(4, 'Prima Edi Napitupulu M.Ak', 'Jln. Gajah Mada No. 743, Palangka Raya 50600, Riau', NULL, NULL),
(5, 'Karman Sihombing', 'Jln. Sutami No. 977, Cimahi 37020, NTB', NULL, NULL),
(6, 'Prabowo Wibisono', 'Jr. Sadang Serang No. 447, Tanjung Pinang 55266, NTT', NULL, NULL),
(7, 'Irma Pertiwi', 'Psr. Pelajar Pejuang 45 No. 190, Serang 58695, JaBar', NULL, NULL),
(8, 'Chelsea Faizah Wastuti S.Farm', 'Jr. Radio No. 997, Administrasi Jakarta Barat 92674, DKI', NULL, NULL),
(9, 'Panca Adriansyah S.E.I', 'Jln. Sutoyo No. 169, Tegal 20599, PapBar', NULL, NULL),
(10, 'Jamalia Susanti M.TI.', 'Kpg. Gajah Mada No. 730, Sabang 90927, JaTim', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Padmi Kamila Hassanah S.Farm', NULL, NULL),
(2, 'Julia Oktaviani', NULL, NULL),
(3, 'Darmana Sitompul', NULL, NULL),
(4, 'Naradi Nainggolan', NULL, NULL),
(5, 'Tedi Winarno', NULL, NULL),
(6, 'Ulya Yani Permata S.Pt', NULL, NULL),
(7, 'Maida Uyainah', NULL, NULL),
(8, 'Putri Dian Nasyidah M.Pd', NULL, NULL),
(9, 'Lantar Uwais', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `telepon`
--

CREATE TABLE `telepon` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor_telepon` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengguna_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `telepon`
--

INSERT INTO `telepon` (`id`, `nomor_telepon`, `pengguna_id`, `created_at`, `updated_at`) VALUES
(1, '(+62) 878 0989 834', 1, NULL, NULL),
(2, '(+62) 509 9868 0557', 2, NULL, NULL),
(3, '023 9503 4379', 3, NULL, NULL),
(4, '(+62) 24 1120 052', 4, NULL, NULL),
(5, '0535 3676 2454', 5, NULL, NULL),
(6, '0614 0945 4128', 6, NULL, NULL),
(7, '0460 8541 5478', 7, NULL, NULL),
(8, '(+62) 713 5497 976', 8, NULL, NULL),
(9, '(+62) 653 4057 294', 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `username`, `roles`, `address`, `phone`, `avatar`, `status`) VALUES
(1, 'Site Administrator', 'administrator@larashop.test', NULL, '$2y$10$I7D7xSDT9TVLoV2.ChKEfOv08/cNEP8zZzCzehO4UEjpNJFfCL0zm', NULL, '2020-03-31 01:08:24', '2020-03-31 01:08:24', 'administrator', '[\"ADMIN\"]', 'Sarmili, Bintaro, Tangerang Selatan', '081554397702', 'saat-ini-tidak-ada-file.png', 'ACTIVE'),
(2, 'Muhammad Elfan Efendi2', 'elfanefendi88@gmail.com', NULL, '$2y$10$r.9GfYStzDl8h6vlLk.YXeCRryV62t71AheAz6prVD1wXcVeQ3NlO', NULL, '2020-04-01 01:41:33', '2020-04-01 20:39:55', 'elfanefendi88', '[\"STAFF\",\"CUSTOMER\"]', 'lj soekarno hatta2', '081554397701', 'avatars/0fk29x4SIu0DytSiy5WElZD0XiHl6b5pbJsvL5pi.png', 'ACTIVE'),
(3, 'Monkey D. Luffy', 'luffy@test.com', NULL, '$2y$10$DqpcL/wMU85MLEOW/ZP1K.ymuAOtaQIW0uUvW6kqd7EyrglaTArRO', NULL, '2020-04-01 01:43:48', '2020-04-01 01:43:48', 'luffy', '[\"STAFF\"]', 'east blue', '123456789', 'avatars/EMlR5djQRj0Dpe8g09oIbln6tICOqrRVk1bjrB2t.jpeg', 'ACTIVE'),
(4, 'sdfs', 'asf@hdfh.com', NULL, '$2y$10$gK8qGnojI.2w63GX5Nqgsu8vMEXgdXzR2kCz7FtgK1yAc5Jxut9TW', NULL, '2020-04-01 20:57:18', '2020-04-01 20:57:18', 'gds', '[\"ADMIN\",\"STAFF\"]', 'er', '435', NULL, 'ACTIVE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_category`
--
ALTER TABLE `book_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_category_book_id_foreign` (`book_id`),
  ADD KEY `book_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `book_order`
--
ALTER TABLE `book_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_order_order_id_foreign` (`order_id`),
  ADD KEY `book_order_book_id_foreign` (`book_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telepon`
--
ALTER TABLE `telepon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_category`
--
ALTER TABLE `book_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `book_order`
--
ALTER TABLE `book_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `telepon`
--
ALTER TABLE `telepon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `book_category`
--
ALTER TABLE `book_category`
  ADD CONSTRAINT `book_category_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `book_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `book_order`
--
ALTER TABLE `book_order`
  ADD CONSTRAINT `book_order_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `book_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
