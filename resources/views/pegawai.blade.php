<html>
<head>
	<title>Tutorial Laravel #20 : Eloquent Laravel</title>
</head>
<body>
 
<h1>Data Pegawai</h1>
<h3>www.malasngoding.com</h3>
 
<h2>All</h2>
<ul>
	@foreach($all as $p)
		<li>{{ "Nama : ". $p->nama . ' | Alamat : ' . $p->alamat }}</li>
	@endforeach
</ul>
 
<h2>First</h2>
{{ 'Nama : '.$first->nama.' | Alamat : '.$first->alamat }}

<h2>Find id 5</h2>
{{ 'Nama : '.$find->nama.' | Alamat : '.$find->alamat }}

<h2>Where nama : Jamalia Susanti M.TI.</h2>
<ul>
	@foreach($where as $p)
		<li>{{ "Nama : ". $p->nama . ' | Alamat : ' . $p->alamat }}</li>
	@endforeach
</ul>

<h2>Where id >= 5</h2>
<ul>
	@foreach($where2 as $p)
		<li>{{ "Nama : ". $p->nama . ' | Alamat : ' . $p->alamat }}</li>
	@endforeach
</ul>

<h2>Where nama like ma</h2>
<ul>
	@foreach($where3 as $p)
		<li>{{ "Nama : ". $p->nama . ' | Alamat : ' . $p->alamat }}</li>
	@endforeach
</ul>
</body>
</html>