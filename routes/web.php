<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('auth.login');
});
Route::get('/percobaan/{nilai1}/test/{nilai2}', 'percobaanController@testing');

Route::get('/customer', function() {
    return view('testcustomer');
})->name('testcustomer');

Route::resource("users", "UserController");

Route::resource('orders', 'OrderController');

Route::get('/books/trash', 'BookController@trash')->name('books.trash');
Route::post('/books/{book}/restore', 'BookController@restore')->name('books.restore');
Route::delete('/books/{id}/delete-permanent', 'BookController@deletePermanent')->name('books.delete-permanent');
Route::resource('books', 'BookController');

Route::get('/ajax/categories/search', 'CategoryController@ajaxSearch');
Route::get('/categories/trash', 'CategoryController@trash')->name('categories.trash');
Route::get('/categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
Route::delete('/categories/{category}/delete-permanent','CategoryController@deletePermanent')->name('categories.deletepermanent');
Route::resource('categories', 'CategoryController');

Route::get('/pegawai', 'PegawaiController@index');

Auth::routes();
Route::match(["GET", "POST"], "/register", function(){
	return redirect("/login");
})->name("register");

Route::get('/home', 'HomeController@index')->name('home');
